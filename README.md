# @247studios/delivery

Code delivery tools for 247studios projects.

`npm install --save-dev @247studios/delivery`

# Usage

```ts
import * as cdk from "@aws-cdk/core";
import { StaticSite } from "@247studios/delivery";

export class MyStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    new StaticSite(this, `StaticSite`, {
      domainName: "example.com",
      subdomainName: "www",
      sourcePath: "../www/dist",
    });
  }
}
```

# Releasing New Versions

Specify semantic version change by running one of `npm version patch`,
`npm version minor`, or `npm version major`.

Once commit reaches the master branch on GitLab, the release will be deployed
to npm automatically.
