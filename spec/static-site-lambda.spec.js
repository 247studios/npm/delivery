/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */
/* eslint-disable */
const path = require("path");
const { handler } = require(path.join(
  require.resolve(`@247studios/delivery`),
  `../constructs/static-site-lambda/index.js`
));

[
  ``,
  `/index.html`,
  `/`,
  `//`,
  `/en.html`,
  `/en`,
  `/en/`,
  `/en//`,
  `/en/about.html`,
  `/en/about`,
  `/en/about/`,
  `/en/index.html`,
  `/en/file.jpg`,
  `/en/file.jpg/`,
  `\\about\\\\`,
].forEach((uri) =>
  handler(
    { Records: [{ cf: { request: { uri } } }] },
    null,
    (err, response) => {
      console.log(
        JSON.stringify(uri),
        `->`,
        JSON.stringify(response, null, 2),
        `\n`
      );
    }
  )
);
