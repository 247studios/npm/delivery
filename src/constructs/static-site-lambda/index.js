/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */
/* eslint-disable */
"use strict";

const escapeRegExp = (text) =>
  text.replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, `\\$&`);

const fileRegExp = /\.[^/]+$/;
const isFileURI = (uri) => fileRegExp.test(uri);

const directoryRegExp = /\/$/;
const isDirectoryURI = (uri) => directoryRegExp.test(uri);

const htmlRegExp = /\.html$/;
const isHTMLURI = (uri) => htmlRegExp.test(uri);

const backSlashesRegExp = /\\+/g;
const adjacentForwardSlashesRegExp = /\/{2,}/g;
const normalizeURI = (uri) =>
  uri
    .replace(backSlashesRegExp, `/`)
    .replace(adjacentForwardSlashesRegExp, `/`);

const index = `index.html`;
const indexRegExp = new RegExp(`${escapeRegExp(index)}$`);

const stripHTMLExtension = (uri) => {
  if (uri.endsWith(index)) {
    return uri.replace(indexRegExp, ``);
  }
  return uri.replace(htmlRegExp, `/`);
};

const appendTrailingSlash = (uri) => `${uri}/`;

const statusDescriptions = {
  "301": `Moved Permanently`,
  "302": `Moved Temporarily`,
};

function redirect(status, request, toURI) {
  const redirectToURI = request.querystring
    ? `${toURI}?${request.querystring}`
    : `${toURI}`;
  return {
    body: ``,
    status: String(status),
    statusDescription: statusDescriptions[status],
    headers: {
      location: [
        {
          key: `Location`,
          value: redirectToURI,
        },
      ],
    },
  };
}

exports.handler = (event, context, callback) => {
  // Extract the request from the CloudFront event that is sent to Lambda@Edge
  const {
    Records: [
      {
        cf: { request },
      },
    ],
  } = event;

  const normalizedURI = normalizeURI(request.uri);
  if (normalizedURI !== request.uri) {
    return callback(null, redirect(301, request, normalizedURI));
  }

  if (isFileURI(request.uri)) {
    if (isHTMLURI(request.uri)) {
      return callback(
        null,
        redirect(301, request, stripHTMLExtension(request.uri))
      );
    }
    return callback(null, request);
  }

  if (isDirectoryURI(request.uri)) {
    return callback(null, {
      ...request,
      uri: request.uri + index,
    });
  }

  // Request is for a file without extension
  // Assume it is a request for directory
  return callback(
    null,
    redirect(301, request, appendTrailingSlash(request.uri))
  );
};
