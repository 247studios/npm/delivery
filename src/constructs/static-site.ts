/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */
import * as path from "path";
import { startCase } from "lodash";
import { Construct, RemovalPolicy } from "@aws-cdk/core";
import { Bucket } from "@aws-cdk/aws-s3";
import {
  CloudFrontWebDistribution,
  SSLMethod,
  SecurityPolicyProtocol,
  PriceClass,
  ViewerProtocolPolicy,
  LambdaEdgeEventType,
  SourceConfiguration,
} from "@aws-cdk/aws-cloudfront";
import { DnsValidatedCertificate } from "@aws-cdk/aws-certificatemanager";
import { PolicyStatement, Effect, AnyPrincipal } from "@aws-cdk/aws-iam";
import { ARecord, RecordTarget, HostedZone } from "@aws-cdk/aws-route53";
import { BucketDeployment, Source } from "@aws-cdk/aws-s3-deployment";
import { CloudFrontTarget } from "@aws-cdk/aws-route53-targets";
import { Function, Runtime, Code, Version } from "@aws-cdk/aws-lambda";

export interface StaticSiteProps {
  /**
   * The apex domain name
   * @example "247studios.ca"
   */
  readonly domainName: string;
  /**
   * The domain name that will host the site.
   * @example "www.247studios.ca"
   */
  readonly recordName: string;
  /**
   * Path to the public directory that will be served
   * @example "../www/public"
   */
  readonly sourcePath: string;
  /**
   * Extra origin configurations to be added to the CloudFront distribution
   */
  readonly originConfigs?: SourceConfiguration[];
}

/**
 * Uploads a static site to S3 and configures a CloudFront Distribution.
 *
 * If you are hosting a site for the www subdomain, you should also use
 * {@link RedirectSite} to redirect the apex domain to the www subdomain.
 * @requires - region to be "us-east-1"
 */
export class StaticSite extends Construct {
  constructor(parent: Construct, name: string, props: StaticSiteProps) {
    super(parent, name);

    const { domainName, recordName, sourcePath, originConfigs = [] } = props;

    const zone = HostedZone.fromLookup(this, `Zone`, {
      domainName,
    });

    const { certificateArn } = new DnsValidatedCertificate(
      this,
      `Certificate`,
      {
        domainName,
        subjectAlternativeNames: [recordName],
        hostedZone: zone,
        // Certificate must be in the us-east-1 region to be used by CloudFront
        // https://github.com/aws/aws-cdk/issues/3464
        region: `us-east-1`,
      }
    );

    const bucket = new Bucket(this, `Bucket`, {
      websiteIndexDocument: `index.html`,
      websiteErrorDocument: `404.html`,
      publicReadAccess: true,
      removalPolicy: RemovalPolicy.DESTROY,
    });
    bucket.addToResourcePolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: [`s3:ListBucket`],
        resources: [bucket.bucketArn],
        principals: [new AnyPrincipal()],
      })
    );

    const rewriteLambdaFunction = new Function(this, `RewriteFunction`, {
      runtime: Runtime.NODEJS_12_X,
      code: Code.fromAsset(path.resolve(__dirname, `./static-site-lambda`)),
      handler: `index.handler`,
    });

    const distribution = new CloudFrontWebDistribution(this, `Distribution`, {
      originConfigs: [
        ...originConfigs,
        {
          behaviors: [
            {
              isDefaultBehavior: true,
              lambdaFunctionAssociations: [
                {
                  eventType: LambdaEdgeEventType.VIEWER_REQUEST,
                  lambdaFunction: new Version(this, `RewriteFunctionVersion`, {
                    lambda: rewriteLambdaFunction,
                  }),
                },
              ],
            },
          ],
          s3OriginSource: {
            s3BucketSource: bucket,
          },
        },
      ],
      aliasConfiguration: {
        acmCertRef: certificateArn,
        names: [recordName],
        sslMethod: SSLMethod.SNI,
        securityPolicy: SecurityPolicyProtocol.TLS_V1_1_2016,
      },
      comment: `Static site for ${recordName}`,
      priceClass: PriceClass.PRICE_CLASS_ALL,
      viewerProtocolPolicy: ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
      errorConfigurations: [
        {
          errorCode: 404,
          responseCode: 404,
          responsePagePath: `/404.html`,
        },
      ],
    });

    new ARecord(this, `Alias${startCase(recordName).replace(/ /g, ``)}`, {
      zone,
      recordName,
      target: RecordTarget.fromAlias(new CloudFrontTarget(distribution)),
    });

    new BucketDeployment(this, `DeployWithInvalidation`, {
      sources: [Source.asset(sourcePath)],
      destinationBucket: bucket,
      distribution,
      distributionPaths: [`/*`],
    });
  }
}
