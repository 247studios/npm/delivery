/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */

import * as _ from "lodash";
import * as cdk from "@aws-cdk/core";
import { Function, FunctionProps } from "@aws-cdk/aws-lambda";
import { LambdaRestApi } from "@aws-cdk/aws-apigateway";
import {
  SourceConfiguration,
  CloudFrontAllowedMethods,
} from "@aws-cdk/aws-cloudfront";
import { Stack } from "@aws-cdk/core";

/**
 * Properties to configure an HTTPS Lambda API
 */
export interface LambdaAPIProps {
  /**
   * Lambda Props
   */
  readonly functionProps: FunctionProps;
  /**
   * Key value pairs that will be exposed as environment variables
   * during the function runtime.
   */
  readonly functionEnvironment?: { [key: string]: string };
}

/**
 * Creates an AWS Lambda API with API Gateway.
 */
export class LambdaAPI extends cdk.Construct {
  private readonly lambdaRestAPI: LambdaRestApi;

  constructor(scope: cdk.Construct, id: string, props: LambdaAPIProps) {
    super(scope, id);

    const { functionProps, functionEnvironment = {} } = props;

    const handler = new Function(this, `Function`, functionProps);

    Object.entries(functionEnvironment).forEach(([key, value]) => {
      handler.addEnvironment(key, value);
    });

    this.lambdaRestAPI = new LambdaRestApi(this, `APIGateway`, {
      handler,
      description: `Lambda API for ${Stack.of(this).stackName}`,
    });
  }

  get hostname(): string {
    return `${this.lambdaRestAPI.restApiId}.execute-api.${
      Stack.of(this).region
    }.amazonaws.com`;
  }

  cloudFrontOriginConfig(
    sourceConfiguration: SourceConfiguration
  ): SourceConfiguration {
    return _.merge<SourceConfiguration, SourceConfiguration>(
      {
        originPath: `/${this.lambdaRestAPI.deploymentStage.stageName}`,
        customOriginSource: {
          domainName: this.hostname,
        },
        behaviors: [
          {
            pathPattern: `/api/*`,
            allowedMethods: CloudFrontAllowedMethods.ALL,
            forwardedValues: {
              cookies: {
                forward: `all`,
              },
              queryString: true,
            },
          },
        ],
      },
      sourceConfiguration
    );
  }
}
