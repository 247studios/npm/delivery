/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */
import { startCase } from "lodash";
import { DnsValidatedCertificate } from "@aws-cdk/aws-certificatemanager";
import {
  CloudFrontWebDistribution,
  OriginProtocolPolicy,
  PriceClass,
  ViewerProtocolPolicy,
} from "@aws-cdk/aws-cloudfront";
import { ARecord, RecordTarget, HostedZone } from "@aws-cdk/aws-route53";
import { CloudFrontTarget } from "@aws-cdk/aws-route53-targets";
import { Bucket, RedirectProtocol } from "@aws-cdk/aws-s3";
import { Construct, RemovalPolicy } from "@aws-cdk/core";

/**
 * Properties to configure an HTTPS Redirect
 */
export interface RedirectSiteProps {
  /**
   * The apex domain name
   * @example "247studios.ca"
   */
  readonly domainName: string;
  /**
   * The redirect target domain
   * @example "www.247studios.ca"
   */
  readonly targetDomain: string;
  /**
   * The domain names to create that will redirect to `targetDomain`
   * @example ["247studios.ca"]
   */
  readonly recordNames: string[];
}

/**
 * Allows creating a domainA -> domainB redirect using CloudFront and S3.
 * You can specify multiple domains to be redirected.
 */
export class RedirectSite extends Construct {
  constructor(scope: Construct, id: string, props: RedirectSiteProps) {
    super(scope, id);

    const { domainName, targetDomain, recordNames } = props;

    const zone = HostedZone.fromLookup(this, `Zone`, {
      domainName,
    });

    const { certificateArn } = new DnsValidatedCertificate(
      this,
      `Certificate`,
      {
        domainName: domainName,
        subjectAlternativeNames: recordNames,
        hostedZone: zone,
        // Certificate must be in the us-east-1 region to be used by CloudFront
        // https://github.com/aws/aws-cdk/issues/3464
        region: `us-east-1`,
      }
    );

    const bucket = new Bucket(this, `Bucket`, {
      websiteRedirect: {
        hostName: targetDomain,
        protocol: RedirectProtocol.HTTPS,
      },
      removalPolicy: RemovalPolicy.DESTROY,
    });

    const distribution = new CloudFrontWebDistribution(this, `Distribution`, {
      defaultRootObject: ``,
      originConfigs: [
        {
          behaviors: [{ isDefaultBehavior: true }],
          customOriginSource: {
            domainName: bucket.bucketWebsiteDomainName,
            originProtocolPolicy: OriginProtocolPolicy.HTTP_ONLY,
          },
        },
      ],
      aliasConfiguration: {
        acmCertRef: certificateArn,
        names: recordNames,
      },
      comment: `Redirect to ${targetDomain} from ${recordNames.join(`, `)}`,
      priceClass: PriceClass.PRICE_CLASS_ALL,
      viewerProtocolPolicy: ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
    });

    recordNames.forEach((domainName) => {
      new ARecord(this, `Alias${startCase(domainName).replace(/ /g, ``)}`, {
        recordName: domainName,
        zone: zone,
        target: RecordTarget.fromAlias(new CloudFrontTarget(distribution)),
      });
    });
  }
}
